import styled from 'styled-components';

const FooterWrapper = styled.section`
  padding: 80px 0 55px;
  margin-top: 40px;
  background-color: #121212;
  @media (max-width: 480px) {
    padding: 60px 0 30px;
  }
  .copyrightClass {
    @media (max-width: 1024px) {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .copyrightMenu {
      @media (max-width: 1024px) {
        margin-top: 10px;
        margin-bottom: 10px;
        justify-content: left;
        align-items: left;
        margin-left: 0;
      }
      @media (max-width: 767px) {
        justify-content: left;
        align-items: left;
        margin-left: 0;
        margin-top: 10px;
        margin-bottom: 10px;
      }
    }
    .copyrightText {
      @media (max-width: 1100px) {
        margin-left: 0;
      }
    }
  }
`;

const List = styled.ul``;

const ListItem = styled.li`
  a {
    color: white;
    font-size: 14px;
    line-height: 36px;
    transition: all 0.2s ease;
    &:hover,
    &:focus {
      outline: 0;
      text-decoration: none;
      color: #343d48;
    }
  }
`;

export const FormWrapper = styled.form`
  font-family: 'Open Sans', sans-serif;
  display: flex;
  flex-direction: column;
  align-items: left;
  justify-content: space-between;
`

export const DefaultTextInput = styled.input`
  width: 150px;
  padding: 0.7em 0.5em;
  margin-top: 1.5em;
  border: 0.2em solid white;
  border-radius: 0.2em;
  background-color: #121212;
`
export const SmallConditionsText = styled.p`
  font-size: 0.8em;
  font-style: oblique;
  font-family: 'Open Sans', sans-serif;
  text-align: left;
  margin-top: 1.5em;
  color: white;
`

export { List, ListItem };

export default FooterWrapper;
