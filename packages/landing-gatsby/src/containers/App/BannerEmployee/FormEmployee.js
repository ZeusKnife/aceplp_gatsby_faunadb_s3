import React from 'react';
import {
    FormWrapper,
    NameWrapper,
    NameInput,
    DefaultTextInput,
    ButtonWrapper,
    SmallConditionsText,
} from './banner.style';

import faunadb from 'faunadb';

export default function FormEmployee() {
    const [firstName, setFirstName] = React.useState('');
    const [lastName, setLastName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [qualification, setQualification] = React.useState('');
    const [nationality, setNationality] = React.useState('');
    const [contactNumber, setContactNumber] = React.useState('');
    const [attachedFileUrl, setAttachedFileUrl] = React.useState('');

    const [attachFilePath, setAttachFilePath] = React.useState('');
    const [attachFileData, setAttachFileData] = React.useState(null);

    var q = faunadb.query;
    var client = new faunadb.Client({
        secret: 'fnADzMuh5UACBfYkVr9-UM81zjRFwCr80x-q4itU',
    });

    // function to submit the data to the database
    const submit = async (event) => {
        event.preventDefault();

        if (!firstName || !lastName || !email) {
            return;
        }

        await client
            .query(
                q.Create(q.Collection('Employee'), {
                    data: {
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        qualification: qualification,
                        nationality: nationality,
                        contactNumber: contactNumber,
                        attachedFileUrl: attachedFileUrl,
                    },
                })
            )
            .then((ret) => console.log(ret));
    };

    const onFileSelect = (event) => {
        event.preventDefault();

        let file = event.target.files[0];

        if (file) {
            const reader = new FileReader(file);
            reader.readAsDataURL(file);

            reader.onload = () => {
                // set image and base64'd image data in componebt state
                setAttachFilePath(file);
                setAttachFileData(reader.result);
            };
        }
    };

    return (
        <div>
            <FormWrapper>
                <h1>Get in Touch</h1>
                <NameWrapper>
                    <NameInput
                        type="text"
                        placeholder="First Name"
                        onChange={(event) => {
                            setFirstName(event.target.value);
                        }}
                    ></NameInput>
                    <NameInput
                        type="text"
                        placeholder="Last Name"
                        onChange={(event) => {
                            setLastName(event.target.value);
                        }}
                    ></NameInput>
                </NameWrapper>
                <DefaultTextInput
                    type="text"
                    placeholder="Contact Number"
                    onChange={(event) => {
                        setContactNumber(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="email"
                    placeholder="Email"
                    onChange={(event) => {
                        setEmail(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="text"
                    placeholder="Highest Qualification"
                    onChange={(event) => {
                        setQualification(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="text"
                    placeholder="Nationality"
                    onChange={(event) => {
                        setNationality(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="file"
                    placeholder="Resume Attatchment"
                    onChange={onFileSelect}
                ></DefaultTextInput>
                <ButtonWrapper onClick={submit}>
                    Submit Resume
                </ButtonWrapper>
                <SmallConditionsText>
                    By signing up, you agree to our Terms of Service and our
                    Privacy Policy
                </SmallConditionsText>
            </FormWrapper>
        </div>
    );
}
