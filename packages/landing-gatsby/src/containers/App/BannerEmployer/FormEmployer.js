import React from 'react';
import {
    FormWrapper,
    NameWrapper,
    NameInput,
    DefaultTextInput,
    FormSubmitButton,
    SmallConditionsText,
} from './banner.style';
import faunadb from 'faunadb';

export default function FormEmployer() {
    const [firstName, setFirstName] = React.useState('');
    const [lastName, setLastName] = React.useState('');
    const [workEmail, setWorkEmail] = React.useState('');
    const [jobTitle, setJobTitle] = React.useState('');
    const [jobFunction, setJobFunction] = React.useState('');
    const [country, setCountry] = React.useState('');
    const [phoneNumber, setphoneNumber] = React.useState('');
    const [searchJob, setSearchJob] = React.useState('');

    var q = faunadb.query;
    var client = new faunadb.Client({
        secret: 'fnADzMuh5UACBfYkVr9-UM81zjRFwCr80x-q4itU',
    });

    // function to submit the data to the database
    const submit = async (event) => {
        event.preventDefault();

        if (!firstName || !lastName || !workEmail) {
            return;
        }

        await client
            .query(
                q.Create(q.Collection('Employer'), {
                    data: {
                        firstName: firstName,
                        lastName: lastName,
                        workEmail: workEmail,
                        jobTitle: jobTitle,
                        jobFunction: jobFunction,
                        country: country,
                        phoneNumber: phoneNumber,
                        searchJob: searchJob,
                    },
                })
            )
            .then((ret) => console.log(ret));
    };

    return (
        <div>
            <FormWrapper>
                <h1>Get in Touch</h1>
                <NameWrapper>
                    <NameInput
                        type="text"
                        placeholder="First Name"
                        onChange={(event) => {
                            setFirstName(event.target.value);
                        }}
                    ></NameInput>
                    <NameInput
                        type="text"
                        placeholder="Last Name"
                        onChange={(event) => {
                            setLastName(event.target.value);
                        }}
                    ></NameInput>
                </NameWrapper>
                <DefaultTextInput
                    type="text"
                    placeholder="Work Email"
                    onChange={(event) => {
                        setWorkEmail(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="text"
                    placeholder="Job Title"
                    onChange={(event) => {
                        setJobTitle(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="text"
                    placeholder="Job Function"
                    onChange={(event) => {
                        setJobFunction(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="text"
                    placeholder="Country"
                    onChange={(event) => {
                        setCountry(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="text"
                    placeholder="Phone Number"
                    onChange={(event) => {
                        setphoneNumber(event.target.value);
                    }}
                ></DefaultTextInput>
                <DefaultTextInput
                    type="text"
                    placeholder="Search Jobs"
                    onChange={(event) => {
                        setSearchJob(event.target.value);
                    }}
                ></DefaultTextInput>
                <FormSubmitButton onClick={submit}>
                    Get a reply in 1 Working Day
                </FormSubmitButton>
                <SmallConditionsText>
                    By signing up, you agree to our Terms of Service and our
                    Privacy Policy
                </SmallConditionsText>
            </FormWrapper>
        </div>
    );
}
