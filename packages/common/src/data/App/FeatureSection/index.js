const data = {
  features: [
    {
      id: 1,
      icon: 'flaticon-atom',
      title: '1. Submit Resume',
      description:
        'Use our pre-defined templates',
    },
    {
      id: 2,
      icon: 'flaticon-trophy',
      title: '2. Wait',
      description:
        'We reach out to a network of clients to look for open job opportunities',
    },
    {
      id: 3,
      icon: 'flaticon-conversation',
      title: '3. Placement',
      description: ' We provide professional support and training to facilitate the onboarding process ',
    },
  ],
};
export default datas;
