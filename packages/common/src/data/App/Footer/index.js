const data = {
  menuWidget: [
    {
      id: 1,
      title: 'Solutions',
      menuItems: [
        {
          id: 1,
          url: '#',
          text: 'BIM Service',
        },
        {
          id: 2,
          url: '#',
          text: 'Facility Management',
        },
        {
          id: 3,
          url: '#',
          text: 'Virtual Design and Construction',
        },
      ],
    },
    {
      id: 2,
      title: 'About Us',
      menuItems: [
        {
          id: 1,
          url: '#',
          text: 'Track Record',
        },
        {
          id: 2,
          url: '#',
          text: 'Careers',
        },
        {
          id: 3,
          url: '#',
          text: 'Blog',
        },
        {
          id: 4,
          url: '#',
          text: 'Events',
        },
      ],
    },
    {
      id: 3,
      title: 'Enquries',
      menuItems: [
        {
          id: 1,
          url: '#',
          text: 'Contact',
        },
        {
          id: 2,
          url: '#',
          text: 'Vendors',
        },
        {
          id: 3,
          url: '#',
          text: 'Partnership',
        },
      ],
    },
  ],
};
export default data;
